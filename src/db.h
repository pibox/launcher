/*******************************************************************************
 * launcher
 *
 * db.h:  Functions for reading a VideoLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_H
#include <gtk/gtk.h>

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * A single app record
 */
typedef struct _app_t {
    char        *next;
    char        *prev;
    char        *appName;
    char        *author;
    char        *contact;
    char        *command;
    char        *description;
    char        *icon;
    GdkPixbuf   *iconImage;
    int         iwidth;
    int         iheight;
    char        *splash;
    GdkPixbuf   *splashImage;
    int         swidth;
    int         sheight;
} APP_T;

typedef struct __rectangle {
    int         width;
    int         height;
    int         splash;
} RECTANGLE_T;

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

// Production location for application description files.
#define DBTOP       "/etc/launcher"

// Test location for local media
#define DBTOP_T     "./data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern int      dbLoad( char *path, int noshow );
extern void     populateList (GtkListStore *listStore);
extern void     updatePoster (GtkTreeSelection *, gpointer);
extern APP_T    *dbGetEntry (gint idx, gint noshow);
extern guint    dbGetLength ( gint noshow );
extern void     computeScale( RECTANGLE_T *canvas, RECTANGLE_T *src);
extern void     resizeIcons(gint width, gint height);
extern void     resizeSplash(gint width, gint height);
extern guint    dbFindEntry ( char *appName, gint noshow );
#endif
