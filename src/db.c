/*******************************************************************************
 * launcher
 *
 * db.c:  Functions for reading and managing a app description files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <glib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <pibox/pibox.h>

#include "launcher.h"

/* Local prototypes. */
guint dbGetLength ( gint noshow );

GSList  *appList   = NULL;
GSList  *appNoShow = NULL;
APP_T   *activeApp = NULL;

/*
 *========================================================================
 * Name:   computeScale
 * Prototype:  void computeScale( GdkRectangle *, GdkRectangle * )
 *
 * Description:
 * Compute a new size for an image based on the canvas it will be drawn in.
 * Save the results back into the source rectangle.
 *
 * Arguments:
 * GdkRectangle *canvas The width/height of the region in which the image will be drawn.
 * GdkRectangle *src    The width/height of and image to be scaled.
 *========================================================================
 */
void
computeScale( RECTANGLE_T *canvas, RECTANGLE_T *src)
{
    gfloat  ri, rs;             // ratios of image and source canvas.
    int     swidth, sheight;    // Scaled width/height

    /* Compute scaling to keep aspect ratio but fit in the canvas area */
    ri = (gfloat)((gfloat)src->width / (gfloat)src->height);
    rs = (gfloat)((gfloat)canvas->width / (gfloat)canvas->height);
    if ( rs > ri )
    {
        swidth = (gint)(src->width * canvas->height/src->height);
        sheight = canvas->height;
    }
    else
    {
        swidth = canvas->width;
        sheight = (gint)(src->height * canvas->width/src->width);
    }

    if ( isCLIFlagSet(CLI_MINI_ICONS) )
    {
        if ( (swidth>src->width) || (sheight>src->height) )
        {
            swidth = src->width;
            sheight = src->height;
        }
    }

    /* Save the updated width/height to the src parameter. */
    src->width = swidth;
    src->height = sheight;
}

/*
 *========================================================================
 * Name:   scaleIcon
 * Prototype:  void scaleIcon( gpointer *, gpointer * )
 *
 * Description:
 * Iterator for each APP_T entry to scale it's icon to fit in the display.
 *========================================================================
 */
static void
scaleIcon( gpointer item, gpointer user_data )
{
    APP_T           *app    = (APP_T *)item;
    RECTANGLE_T     *canvas = (RECTANGLE_T *)user_data;
    RECTANGLE_T     src;
    GdkPixbuf       *image;
    GdkPixbuf       *newimage;

    /* Choose which image to use on this platform. */
    if ( !canvas->splash )
    {
        if ( ((cliOptions.platform==PIBOX_RPI2) ||
              (cliOptions.platform==PIBOX_RPI3) ||
              (cliOptions.platform==PIBOX_RPI4) ||
              (cliOptions.platform==PIBOX_PPP)) && (app->splashImage != NULL) )
        {
            image = app->splashImage;
            src.width = app->swidth;
            src.height = app->swidth;
            piboxLogger(LOG_TRACE1, "Using splash %s for app icon %s\n", app->splash, app->appName);
        }
        else
        {
            if ( app->iconImage == NULL )
            {
                piboxLogger(LOG_ERROR, "Missing icon image for app %s\n", app->appName);
                return;
            }
            image = app->iconImage;
            src.width = app->iwidth;
            src.height = app->iwidth;
            piboxLogger(LOG_TRACE1, "Using icon %s for app %s\n", app->icon, app->appName);
        }
    }
    else
    {
        image = app->splashImage;
        src.width = app->swidth;
        src.height = app->swidth;
        piboxLogger(LOG_TRACE1, "Scaling splash %s for app %s\n", app->splash, app->appName);
    }

    /* Compute new dimensions for icon. */
    computeScale(canvas, &src);

    /* Scale the image. */
    piboxLogger(LOG_TRACE2, "updated image size: %d / %d\n", src.width, src.height);
    newimage = gdk_pixbuf_scale_simple(image, src.width, src.height, GDK_INTERP_BILINEAR);

    /* Save the updated image and size. */
    if ( !canvas->splash )
    {
        g_object_unref(app->iconImage);
        app->iconImage = newimage;
        app->iwidth = src.width;
        app->iheight = src.height;
    }
    else
    {
        g_object_unref(app->splashImage);
        app->splashImage = newimage;
        app->swidth = src.width;
        app->sheight = src.height;
    }
}

/*
 *========================================================================
 * Name:   appSorter
 * Prototype:  gint appSorter( gconstpointer, gconstpointer )
 *
 * Description:
 * A Comparison function for inserting apps by name alphabetically.
 * 
 * Notes:
 * First argument is an existing list element.  Second argument is the app
 * to be added to the list.
 *========================================================================
 */
gint 
appSorter (gconstpointer app1, gconstpointer app2)
{
    APP_T *existingApp = (APP_T *)app1;
    APP_T *newApp = (APP_T *)app2;
    if ( app1 == NULL )
        return 1;
    if ( app2 == NULL )
        return 0;
    return ((gint) strcasecmp ((char *)existingApp->appName, (char *)newApp->appName));
}

/*
 *========================================================================
 * Name:   freeAppEntry
 * Prototype:  void freeAppEntry( APP_T *ptr )
 *
 * Description:
 * Free up alllocated storage for an app entry.
 *========================================================================
 */
void 
freeAppEntry (APP_T *ptr)
{
    if ( ptr == NULL )
        return;
    if ( ptr->appName != NULL ) g_free(ptr->appName);
    if ( ptr->author != NULL ) g_free(ptr->author);
    if ( ptr->contact != NULL ) g_free(ptr->contact);
    if ( ptr->command != NULL ) g_free(ptr->command);
    if ( ptr->icon != NULL ) g_free(ptr->icon);
    if ( ptr->splash != NULL ) g_free(ptr->splash);
    if ( ptr->description != NULL ) g_free(ptr->description);
}

/*
 *========================================================================
 * Name:   parseElement
 * Prototype:  void parseElement( xmlDocPtr doc, xmlNodePtr *ptr )
 *
 * Description:
 * Parse the property from an XML element.
 *========================================================================
 */
char *
parseElement ( xmlDocPtr doc, xmlNodePtr cur, char *propName )
{
    xmlChar *key;
    char *val = NULL;
    char *ptr1, *ptr2;

    key = xmlNodeListGetString (doc, cur->xmlChildrenNode, 1);
    if ( key != NULL )
    {
        val = g_strdup( (char *)key );
        ptr1 = piboxTrim( val );
        trimReverse( (char *)(val + strlen(val)-1));
        ptr2 = g_strdup( ptr1 );
        g_free(val);
        val = ptr2;
        piboxLogger(LOG_INFO, "Key = *%s*\n", val);
    }
    xmlFree ( key );
    return val;
}

/*
 *========================================================================
 * Name:   resizeIcons
 * Prototype:  void resizeIcons( gint width, gint height )
 *
 * Description:
 * Iterate over all app entries to resize their icons to fit the display.
 * Resized image may be either their icon or their splash, depending on
 * platform type.
 *========================================================================
 */
void
resizeIcons(gint width, gint height)
{
    RECTANGLE_T canvas;
    canvas.width = width;
    canvas.height = height;
    canvas.splash = 0;
    piboxLogger(LOG_INFO, "Number of app icons to resize: %d\n", dbGetLength(0) );
    g_slist_foreach( appList, scaleIcon, &canvas );
}

/*
 *========================================================================
 * Name:   resizeSplash
 * Prototype:  void resizeSplash( gint width, gint height )
 *
 * Description:
 * Iterate over all app entries to resize their splash images to fit the display.
 * Resized image may be either their icon or their splash, depending on
 * platform type.
 *========================================================================
 */
void
resizeSplash(gint width, gint height)
{
    RECTANGLE_T canvas;
    canvas.width = width;
    canvas.height = height;
    canvas.splash = 1;
    piboxLogger(LOG_INFO, "Resizing splash images to %d x %d\n", width, height);
    g_slist_foreach( appList, scaleIcon, &canvas );
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( char *path, int noshow )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Arguments:
 * char *path           Path to review for XML configurations.
 * int  noshow          If set, parse into the noshow list instead of the active list.
 *
 * Returns:
 * -1 on failure, 0 otherwise.
 *========================================================================
 */
int 
dbLoad(char *appDir, int noshow)
{
    APP_T           *app = NULL;
    xmlDocPtr       doc;
    xmlNodePtr      cur_node;
    char            *msg = NULL;

    DIR             *dir = NULL;
    struct dirent   *entry = NULL;
    int             cnt = 0;

    // Find all configuration files in app directory.
    dir = opendir(appDir);
    if ( dir == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory: %s\n", appDir);
        return -1;
    }

    // For each file in the directory, parse as XML.
    while( (entry=readdir(dir)) != NULL )
    {
        if ( entry->d_name[0] == '.' )
            continue;

        // We were designed to run on Linux, so this should work.
        if ( entry->d_type == DT_DIR )
            continue;
        piboxLogger(LOG_INFO, "App Description file: %s\n", entry->d_name);

        // Read file and parse as xml 
        doc = xmlReadFile(entry->d_name, NULL, XML_PARSE_NOERROR);
        if ( doc == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to parse %s\n", entry->d_name);
            continue;
        }
        cur_node=xmlDocGetRootElement(doc);
        if ( cur_node == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to get root node.\n", entry->d_name);
            continue;
        }

        // Create an APP_T instance
        app = g_new0(APP_T, 1);

        // Run the document tree and extract fields of interest into
        // the APP_T instance.  This is very simple: one element per config item.
        for(cur_node=cur_node->xmlChildrenNode; cur_node; cur_node=cur_node->next)
        {
            if (cur_node->type != XML_ELEMENT_NODE)
                continue;

            piboxLogger(LOG_INFO, "Node name: %s\n", cur_node->name);
            if ( xmlStrcasecmp(cur_node->name, (xmlChar *)"launcher") == 0 )
                continue;

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "appName") == 0 )
                app->appName = parseElement(doc, cur_node, "appName");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Author") == 0 )
                app->author = parseElement(doc, cur_node, "Author");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Contact") == 0 )
                app->contact = parseElement(doc, cur_node, "Contact");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Command") == 0 )
                app->command = parseElement(doc, cur_node, "Command");

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Icon") == 0 )
            {
                app->icon = parseElement(doc, cur_node, "Icon");
                app->iconImage = gdk_pixbuf_new_from_file(app->icon, NULL);
                app->iwidth = gdk_pixbuf_get_width(app->iconImage);
                app->iheight = gdk_pixbuf_get_height(app->iconImage);
            }

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Splash") == 0 )
            {
                app->splash = parseElement(doc, cur_node, "Splash");
                app->splashImage = gdk_pixbuf_new_from_file(app->splash, NULL);
                app->swidth = gdk_pixbuf_get_width(app->splashImage);
                app->sheight = gdk_pixbuf_get_height(app->splashImage);
            }

            else if ( xmlStrcasecmp(cur_node->name, ( const xmlChar * ) "Description") == 0 )
                app->description = parseElement(doc, cur_node, "Description");
        }

        // Validate the entry is complete
        if ( app->appName == NULL ) msg = "Missing appName.";
        else if ( app->author == NULL ) msg = "Missing author.";
        else if ( app->contact == NULL ) msg = "Missing contact.";
        else if ( app->command == NULL ) msg = "Missing command.";
        else if ( app->icon == NULL ) msg = "Missing icon.";
        else if ( app->splash == NULL ) msg = "Missing splash.";
        else if ( app->description == NULL ) msg = "Missing description.";

        // Clean up description
        piboxStripNewline( app->description );

        if ( msg != NULL )
        {
            piboxLogger(LOG_ERROR, "Incomplete description file: %s\n", entry->d_name);
            piboxLogger(LOG_ERROR, "%s\n", msg);
            freeAppEntry(app);
            g_free(app);
            continue;
        }
        
        // Add it to our link list.
        piboxLogger(LOG_INFO, "Loaded app: %s\n", app->appName);
        if ( noshow )
            appNoShow = g_slist_insert_sorted(appNoShow, app, appSorter);
        else
            appList = g_slist_insert_sorted(appList, app, appSorter);

        cnt++;
    }
    closedir(dir);

    piboxLogger(LOG_INFO, "Application descriptions found: %d\n", cnt);

    return cnt;
}

/*
 *========================================================================
 * Name:   dbGetEntry
 * Prototype:  void dbGetEntry( gint idx, gint noshow )
 *
 * Description:
 * Retrieve the app entry at the specified index.
 *
 * Arguments:
 * gint idx     Index into link list to retrieve.
 * gint noshow  If set, search the noshow list instead of active list.
 *========================================================================
 */
APP_T *
dbGetEntry (gint idx, gint noshow)
{
    if ( noshow )
        return g_slist_nth_data(appNoShow, idx);
    else
        return g_slist_nth_data(appList, idx);
}

/*
 *========================================================================
 * Name:   dbGetLength
 * Prototype:  guint dbGetLength( gint noshow )
 *
 * Description:
 * Retrieve the app entry at the specified index.
 *
 * Arguments:
 * gint noshow  If set, search the noshow list instead of active list.
 *========================================================================
 */
guint
dbGetLength ( gint noshow )
{
    if ( noshow )
        return g_slist_length(appNoShow);
    else
        return g_slist_length(appList);
}

/*
 *========================================================================
 * Name:   dbFindEntry
 * Prototype:  guint dbFindEntry( char *appName, gint noshow )
 *
 * Description:
 * Retrieve the index to the app entry with the specified appName.
 *
 * Arguments:
 * gint noshow  If set, search the noshow list instead of active list.
 *
 * Returns:
 * The index into the selected listed that matches or -1 if not found.
 *========================================================================
 */
guint
dbFindEntry ( char *appName, gint noshow )
{
    GSList *list     = appList;
    GSList *iterator = NULL;
    APP_T  *app      = NULL;
    int     idx      = 0;

    if ( noshow )
        list = appNoShow;

    for (iterator = list; iterator; iterator = iterator->next)
    {
        app = (APP_T *)iterator->data;
        piboxLogger(LOG_TRACE1, "Current appName is '%s'\n", app->appName);
        if ( strcmp(app->appName, appName) == 0 )
        {
            piboxLogger(LOG_INFO, "Found appName match!\n");
            return idx;
        }
        idx++;
    }
    return(-1);
}

