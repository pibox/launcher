/*******************************************************************************
 * launcher
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <linux/if_link.h>

#include "launcher.h"

/*
 *========================================================================
 * Name:   trimReverse
 * Prototype:  char *trimReverse( char * )
 *
 * Description:
 * Trim whitespace from the end of a buffer by replacing spaces with \0.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 *========================================================================
 */
void
trimReverse( char *ptr )
{
    // Trim leading space
    while(isspace(*ptr)) 
    {
        *ptr = '\0';
        ptr--;
    }
}

/*
 *========================================================================
 * Name:   isNetworkUp
 * Prototype:  int isNetworkUp( void )
 *
 * Description:
 * Check if there are any IP addresses configured for this system.
 *
 * Returns:
 * 0 if no IP addresses are found or checks cannot be completed.
 * 1 if IP addresses are found.
 *
 * Notes:
 * Only checks for IPv4.
 *
 * Based on example code in getifaddrs(3).
 *========================================================================
 */
int
isNetworkUp( void )
{
    struct ifaddrs  *ifaddr;
    int             family, s;
    char            host[NI_MAXHOST];
    int             status = 0;

    if (getifaddrs(&ifaddr) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed check for network IPs: %s\n", strerror(errno));
        return(status);
    }

    /* Walk list looking for any IPv4 addresses. */
    for (struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;
        if (strcmp(ifa->ifa_name,"lo") == 0)
            continue;

        family = ifa->ifa_addr->sa_family;

        /* For an AF_INET* interface address, display the address */
        if (family == AF_INET)
        {
            s = getnameinfo(ifa->ifa_addr,
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                break;
            }

            /* Minimal host address is 0.0.0.0, plus string termination. */
            piboxLogger(LOG_TRACE3, "Found IP address: %s: %s\n", ifa->ifa_name, host);
            if ( strlen(host) > 7 )
            {
                status=1;
                break;
            }
        }
    }

    freeifaddrs(ifaddr);
    return(status);
}
