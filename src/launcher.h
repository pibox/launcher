/*******************************************************************************
 * launcher
 *
 * launcher.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <gtk/gtk.h>

/* 
 * ========================================================================
 * Type definitions and Data structures
 * =======================================================================
 */

/*
 * An icon and it's metadata.
 */
typedef struct _icon_t {
    GdkPixbuf   *iconImage;
    int         iwidth;
    int         iheight;
} ICON_T;

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "launcher"
#define MAXBUF      4096
#define APPMGR_PORT 13912
#define SPLASH_W    578
#define SPLASH_H    578
#define STATUS_H    30
#define TEXT_H      30

#define MASK_T      "Mask.png"
#define MASK        "/etc/launcher/Mask.png"
#define UNMASK_T    "Unmask.png"
#define UNMASK      "/etc/launcher/Unmask.png"
#define LAUNCHER_ENV_T     "data/pibox-env"

#define T_NETWORK_OFF      "icons/status-bar/network-off.png"
#define T_NETWORK_ON       "icons/status-bar/network-on.png"
#define I_NETWORK_OFF      "/etc/launcher/icons/network-off.png"
#define I_NETWORK_ON       "/etc/launcher/icons/network-on.png"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef LAUNCHER_C
int     daemonEnabled = 0;
char    errBuf[256];
gchar   *imagePath = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
#endif /* LAUNCHER_C */

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/utils.h>
#include "db.h"
#include "cli.h"
#include "utils.h"

#endif /* !LAUNCHER_H */

