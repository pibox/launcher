/*******************************************************************************
 * launcher
 *
 * launcher.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define LAUNCHER_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pigtk/pigtk-textline.h>

#include "launcher.h" 

static gint largeFont  = 25;
static gint mediumFont = 18;
// static gint smallFont  = 10;

gint window_expose_id;
gint window_keypress_id;
gint status_expose_id;
gint splash_realize_id;

int maxApps = 0;
// GdkPixbuf *image = NULL;
cairo_t *cr = NULL;

/*
 * darea is a drawing area for each icon on the launcher.
 * Space for these is allocated at widget creation time.
 */
GtkWidget **darea;

GdkPixbuf *maskImage = NULL;
RECTANGLE_T maskRect;
GtkWidget *statusArea;
GtkWidget *iconTable = NULL;
GtkWidget *appSplash = NULL;
GtkTextBuffer *appDesc = NULL;
guint cellWidth = 0;
guint cellHeight = 0;
guint splashWidth = 0;
guint splashHeight = 0;
guint iconPad = 15;
guint clockID;

typedef struct _display_dimensions {
    int     width;
    int     height;
} DISPLAY_DIMENSIONS_T;

DISPLAY_DIMENSIONS_T workarea = {0};
ICON_T icons[2];

int currentIdx = 0;

int enableTouch = 1;
int inprogress = 0;
int network_avail = 0;

char msgBuf[256];

static pthread_mutex_t networkMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t msgMutex = PTHREAD_MUTEX_INITIALIZER;

/* Local prototypes */
static gboolean draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static gboolean draw_status(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
gboolean iconTouchGTK( gpointer data );
void selectApp( int idx );
void notify(gint idx, gint noshow);
static gboolean timer_exec(GtkWidget * window);
static gboolean status_exec(GtkWidget * window);

/*
 *========================================================================
 * Name:   clearMsg
 * Prototype:  static gboolean clearMsg( void * )
 *
 * Description:
 * Timer callback to clear a status bar message.
 *========================================================================
 */
static gboolean
clearMsg(void *unused)
{
    /* Reset the message and let the time display again. */
    pthread_mutex_lock(&msgMutex);
    memset(msgBuf, 0, 256);
    pthread_mutex_unlock(&msgMutex);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   msg
 * Prototype:  static void msg( char *str )
 *
 * Description:
 * Set a status bar message.  Message should replace clock for 3 seconds.
 *
 * Input Arguments:
 * char *str        The string to show, replacing the time in the status bar.
 * int timeout      The number of seconds to show the message before clearing it.
 *========================================================================
 */
static void
msg( char *str, int timeout )
{
    /* Clear message buffer */
    pthread_mutex_lock(&msgMutex);
    memset(msgBuf, 0, 256);
    strncpy(msgBuf, str, 255);
    pthread_mutex_unlock(&msgMutex);
    g_timeout_add(timeout*1000, (GSourceFunc)clearMsg, NULL);
}

/*
 *========================================================================
 * Name:   iconTouch
 * Prototype:  void iconTouch( int region )
 *
 * Description:
 * Handler for touch location reports. 
 *========================================================================
 */
void
iconTouch( REGION_T *region )
{
    g_idle_add( (GSourceFunc)iconTouchGTK, (gpointer)region );
}

/*
 * Don't notify appManager till we've allowed all touch processing to drain.
 */
static gboolean
disableInProgress(GtkWidget * window)
{
    notify(currentIdx, 0);
    inprogress = 0;
    return(FALSE);
}

gboolean
iconTouchGTK( gpointer data )
{
    int                 idx = 0;
    gint                wx, wy;
    gint                posX, posY;
    REGION_T            *region = (REGION_T *)data;
    GtkRequisition      req;

    piboxLogger(LOG_INFO, "Entered.\n");

    if ( !enableTouch )
    {
        piboxLogger(LOG_INFO, "Touch processing disabled, skipping update\n");
        return FALSE;
    }

    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return FALSE;
    }
    inprogress = 1;
    piboxLogger(LOG_INFO, "Processing touch request.\n");

    /* Refresh the display to show a touch has occurred. */
    selectApp(currentIdx);

    /* Find the widget that maps to the absolute coordinates */
    while( darea[idx] != NULL )
    {
        /* Get the absolute screen coordinates of the widget */
        gdk_window_get_origin (gtk_widget_get_window (darea[idx]), &wx, &wy);
        req.width = gdk_window_get_width(darea[idx]->window);
        req.height = gdk_window_get_height(darea[idx]->window);
        piboxLogger(LOG_INFO, "origin, wxh: %d %d  %d / %d\n", wx, wy, req.width, req.height);

        /*
         * Set up depending on if we are using rotation or not.
         * We only support CCW rotation currently.
         */
        if ( isCLIFlagSet( CLI_ROTATE) )
        {
            piboxLogger(LOG_INFO, "Rotation is enabled.\n");
            posY = region->x;
            posX = workarea.height - region->y;
        }
        else
        {
            posX = region->x;
            posY = region->y;
        }
        piboxLogger(LOG_INFO, "touch point, x=%d y=%d\n", posX, posY);

        /* Test if the event was within the bounds of the widget area. */
        if ( (posX >= wx) && (posX<= (wx+req.width)) )
        {
            if ( (posY >= wy) && (posY <= (wy+req.height)) )
            {
                /* Found widget! */
                piboxLogger(LOG_INFO, "Identified table cell: %d\n", idx);
                selectApp(idx);
                enableTouch = 0;
                g_timeout_add(1500, (GSourceFunc)disableInProgress, NULL);
                return(FALSE);
            }
        }
        idx++;
    }

    inprogress = 0;
    piboxLogger(LOG_INFO, "No matching table cell identified for %d / %d\n", region->x, region->y);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be
 * handled.
 *
 * Notes:
 * Format is
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int             width=0;
    int             height=0;
    GdkScreen       *screen;
    gint            monitor;
    GdkRectangle    dest;

    /* Display dimensions may be set through install proces. */
    // piboxLoadDisplayConfig();
    // width = piboxGetDisplayWidth();
    // height = piboxGetDisplayHeight();

    /* If it's not, then ask the fb for the display size. */
    if  (width == 0 )
    {
        piboxGetDisplaySize ( &width, &height );
        piboxLogger(LOG_INFO, "Width x Height (fb): %d x %d\n", width, height);
    }

    /* Finally, try asking X.org. */
    if  (width == 0 )
    {
        screen = gdk_screen_get_default();
        monitor = gdk_screen_get_primary_monitor(screen);
        gdk_screen_get_monitor_geometry (screen, monitor, &dest);
        width = dest.width;
        height = dest.height;
    }

    /* If requested, use specified geometry. */
    if ( cliOptions.geometryW != 0 )
    {
        width = cliOptions.geometryW;
        height = cliOptions.geometryH;
    }

    /* By now we must have found the real display dimensions.  We hope. */
    workarea.width = width;
    workarea.height = height;
    piboxLogger(LOG_INFO, "Display dimensions: %d x %d.\n", width, height);

    /* Check if we're on a "small" screen. */
    if ( (width<=800) || (height<=480) || (piboxGetDisplayType() == PIBOX_TFT) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    /* Reduce library call overhead with a flag. */
    if ( piboxGetDisplayType()==PIBOX_TFT )
        setCLIFlag(CLI_TFT);

    /* Now test if we're on a touchscreen. */
    if ( piboxGetDisplayTouch() ||
         (piboxGetDisplayType() == PIBOX_LCD) ||
         (piboxGetDisplayType() == PIBOX_TFT) )
    {
        setCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Touch screen support enabled.\n");
#ifdef PIPLAYER
        iconPad = 18;
#else
        iconPad = 9;
#endif
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Not a touch screen.\n");
    }
}

/*
 *========================================================================
 * Name:   generateIconHighlight
 * Prototype:  void generateIconHighlight()
 *
 * Description:
 * Generate the highlight that backs the selected app in the display.
 *========================================================================
 */
void
generateIconHighlight()
{
    RECTANGLE_T         canvas;
    char                maskPath[256];
    APP_T               *appEntry;
    GdkPixbuf           *newimage;

    piboxLogger(LOG_TRACE2, "Generating initial mask image.\n");
    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(maskPath, "%s", MASK_T);
    else
        sprintf(maskPath, "%s", MASK);
    maskImage  = gdk_pixbuf_new_from_file(maskPath, NULL);

    /* Resize the highlight based on an app's icon, which we've already scaled to fit. */
    canvas.width = cellWidth;
    canvas.height = cellHeight;
    appEntry = dbGetEntry(0,0);
#ifndef PIPLAYER
    maskRect.width = appEntry->iwidth;
    maskRect.height = appEntry->iheight;
#else
    maskRect.width = appEntry->iwidth+5;
    maskRect.height = appEntry->iheight+5;
#endif
    computeScale( &canvas, &maskRect);
    maskRect.width  += iconPad;
    maskRect.height += iconPad;

    /* Now regenerate the highlight (re: mask) at the scaled size. */
    piboxLogger(LOG_TRACE2, "Recomputed highlight size: %d / %d\n", maskRect.width, maskRect.height);
    newimage = gdk_pixbuf_scale_simple(maskImage, maskRect.width, maskRect.height, GDK_INTERP_BILINEAR);
    g_object_unref(maskImage);
    maskImage = newimage;
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, APP_T *, int, int )
 *
 * Description:
 * Updates the poster area of the display.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * GdkPixbuf *      The GdkPixbuf image to fill in the drawing.
 * int              If TRUE, position splash.  Otherwise position icon.
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 *========================================================================
 */
void 
do_drawing( GtkWidget *drarea, APP_T *app, int doSplash, int doHighlight)
{
    GdkPixbuf           *image;
    GdkRectangle        imageRect;          /* width and height of an image. */
    GdkRectangle        canvas;             /* drawing widget width and height */
    gdouble             offset_x, offset_y;
    GtkRequisition      req;

    cr = gdk_cairo_create(drarea->window);
    if ( !doSplash )
    {
        req.width = cellWidth;
        req.height = cellHeight;
    }
    else
    {
        req.width = gdk_window_get_width(drarea->window);
        req.height = gdk_window_get_height(drarea->window);
    }

    canvas.width = (int)(req.width * .9);
    canvas.height = (int)(req.width * .9);
    piboxLogger(LOG_TRACE2, "window w/h: %d / %d\n", req.width, req.height);
    piboxLogger(LOG_TRACE2, "adjusted window w/h: %d / %d\n", canvas.width, canvas.height);

    /* Clear the splash area */
    if ( doHighlight == 0 )
    {
        piboxLogger(LOG_TRACE2, "Clearing splash\n");
        cairo_set_source_rgba(cr, 0.13, 0.13, 0.13, 1.0);
        cairo_rectangle(cr, 0, 0, req.width, req.height);
        cairo_fill(cr); 
        cairo_paint(cr);    
    }

    if ( app == NULL )
        goto do_drawing_exit;

    /* Choose the image to work with. */
    if ( doSplash )
    {
        if ( app->splashImage == NULL )
        {
            piboxLogger(LOG_ERROR, "Missing splash image for app %s\n", app->appName);
            return;
        }
        image = app->splashImage;
        imageRect.width = app->swidth;
        imageRect.height = app->sheight;
    }
    else
    {
        if ( app->iconImage == NULL )
        {
            piboxLogger(LOG_ERROR, "Missing icon image for app %s\n", app->appName);
            return;
        }
        image = app->iconImage;
        imageRect.width = app->iwidth;
        imageRect.height = app->iheight;
        piboxLogger(LOG_TRACE1, "Using icon %s for app %s\n", app->icon, app->appName);
    }

    /*
     * If requested, draw a background highlight.
     * The highlight image will be pre-scaled to fit on first use.
     */
    if ( doHighlight == 1 )
    {
        /* Positioning */
        piboxLogger(LOG_TRACE2, "Adding a highlight.\n");
        offset_x = (req.width - maskRect.width) / 2.0;
        offset_y = (req.height - maskRect.height) / 2.0;
        gdk_cairo_set_source_pixbuf(cr, maskImage, offset_x, offset_y);

        cairo_paint(cr);    
    }

    // Or clear the highlight
    else if ( doHighlight == 2 )
    {
        /* Only do this if we've drawn the mask at least once. */
        piboxLogger(LOG_TRACE2, "Clearing highlight\n");
        offset_x = (req.width - maskRect.width) / 2.0;
        offset_y = (req.height - maskRect.height) / 2.0;
        cairo_set_source_rgba(cr, 0.13, 0.13, 0.13, 1.0);
        cairo_rectangle(cr, offset_x, offset_y, (double)maskRect.width, (double)maskRect.height);
        cairo_fill(cr); 
        cairo_paint(cr);    
    }

    /* Now display the image, scaled if necessary. */
    if ( doSplash )
    {
        piboxLogger(LOG_TRACE2, "Displaying the splash: %s\n", app->appName);

        /* Positioning */
        offset_x = (double)(req.width - imageRect.width) / (double)2.0;
        offset_y = (double)(req.height - imageRect.height) / (double)2.0;
        gdk_cairo_set_source_pixbuf(cr, image, offset_x, offset_y);
    }
    else
    {
        /* Positioning */
        piboxLogger(LOG_TRACE2, "Displaying the icon: %s\n", app->appName);
        piboxLogger(LOG_TRACE2, "req w/h: %d / %d\n", req.width, req.height);
        piboxLogger(LOG_TRACE2, "image w/h: %d / %d\n", app->iwidth, app->iheight);
        offset_x = (double)(req.width - app->iwidth) / (double)2.0;
        offset_y = (double)(req.height - app->iheight) / (double)2.0;
        piboxLogger(LOG_TRACE2, "offset x/y: %d / %d\n", offset_x, offset_y);
        piboxLogger(LOG_TRACE2, "image: %08x \n", (char *)image);
        gdk_cairo_set_source_pixbuf(cr, image, offset_x, offset_y);
    }

do_drawing_exit:
    cairo_paint(cr);    
    cairo_destroy(cr);
    cr = NULL;
}

/*
 *========================================================================
 * Name:   draw_status
 * Prototype:  void draw_status( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the status area at the top of the launcher.
 *========================================================================
 */
static gboolean
draw_status(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    gint                    fontSize = largeFont;
    time_t                  currentTime;
    struct tm               *tm;
    GtkRequisition          req;
    GdkPixbuf               *network_image;
    gdouble                 offset_x, offset_y;

    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        fontSize = mediumFont;
    piboxLogger(LOG_TRACE3, "text size: %d\n", fontSize);

    req.width = gdk_window_get_width(statusArea->window);
    req.height = gdk_window_get_height(statusArea->window);

    cr = gdk_cairo_create(statusArea->window);

    /* Clear the window. */
    cairo_set_source_rgba(cr, 0.13, 0.13, 0.13, 1.0);
    cairo_rectangle(cr, 0, 0, req.width, req.height);
    cairo_fill(cr);
    cairo_paint(cr);

    /* Draw network icons */
    pthread_mutex_lock(&networkMutex);
    network_image = icons[network_avail].iconImage;
    offset_x = (double)(req.width) - (double)(icons[network_avail].iwidth + 10);
    offset_y = (double)(req.height - icons[network_avail].iheight) / (double)2.0;
    pthread_mutex_unlock(&networkMutex);
    gdk_cairo_set_source_pixbuf(cr, network_image, offset_x, offset_y);
    cairo_paint(cr);

    if ( network_avail )
    {
        /* Setup text layout */
        layout = pango_cairo_create_layout(cr);
        sprintf(buf, "Monospace Bold %d", fontSize);
        desc = pango_font_description_from_string(buf);
        pango_font_description_set_absolute_size(desc, fontSize*PANGO_SCALE);
        pango_layout_set_font_description(layout, desc);
        pango_font_description_free(desc);
        pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
        pango_layout_set_width(layout, ((int)(workarea.width)-20)*PANGO_SCALE);
        pango_layout_set_spacing(layout, 1);
        pango_layout_set_single_paragraph_mode(layout, FALSE);
        pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);

        /* Initialize attributes. */
        attrs = pango_attr_list_new();
        pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
        pango_layout_set_attributes (layout, attrs);

        /* Get current time as formatted text string. */
        pthread_mutex_lock(&msgMutex);
        if ( strlen(msgBuf) > 0 )
        {
            /* Display a status message. */
            memset(buf, 0, 256);
            strncpy(buf, msgBuf, 255);
        }
        else
        {
            /* Get current time as formatted text string. */
            currentTime = time(NULL);
            tm = localtime(&currentTime);
            strftime(buf, 256, "%b %d %Y  %I:%M:%S", tm);
        }
        pthread_mutex_unlock(&msgMutex);

        /* Draw the text */
        piboxLogger(LOG_TRACE4, "text: %s\n", buf);
        pango_layout_set_text(layout, buf, -1);
        pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
        piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
        cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
        pango_cairo_update_layout(cr, layout);

        cairo_translate(cr, (req.width/2)-(pangoRectangle.width/2), (req.height/2)-(pangoRectangle.height/2));

        pango_cairo_show_layout(cr, layout);

        /* Cleanup pango */
        pango_attr_list_unref(attrs);
        g_object_unref(layout);
    }

    cairo_destroy(cr);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   draw_icon
 * Prototype:  void draw_icon( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the icon area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean 
draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{      
    GtkWidget       *drarea;
    APP_T           *appEntry;
    gint            idx = GPOINTER_TO_INT(user_data);

    piboxLogger(LOG_TRACE2, "Entered.\n");

    // Get the app entry for this index
    appEntry = dbGetEntry(idx,0);
    // if (appEntry == NULL )
    //     return FALSE;

    // Get drawing area widget
    drarea = darea[idx];

    if (appEntry != NULL )
        piboxLogger(LOG_TRACE2, "Calling do_drawing for %s.\n", appEntry->appName);
    else
        piboxLogger(LOG_TRACE2, "Calling do_drawing without app.\n");
    do_drawing(drarea, appEntry, FALSE, 0);
    return(FALSE);
}

#ifndef XEON
#ifndef PIPLAYER
/*
 *========================================================================
 * Name:   draw_splash
 * Prototype:  void draw_splash( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean 
draw_splash(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{      
    APP_T           *appEntry;

    piboxLogger(LOG_TRACE2, "Entered.\n");

    // Get the app entry for this index
    appEntry = dbGetEntry(currentIdx,0);
    if (appEntry == NULL )
        return FALSE;

    piboxLogger(LOG_TRACE2, "Calling do_drawing for %s.\n", appEntry->appName);
    do_drawing(appSplash, appEntry, TRUE, 0);
    return(FALSE);
}
#endif
#endif

/*
 *========================================================================
 * Name:   selectApp
 * Prototype:  void selectApp( gint idx )
 *
 * Description:
 * Fills in the description for the selected app.
 *========================================================================
 */
void 
selectApp( int idx )
{
    APP_T           *appEntry;

    piboxLogger(LOG_INFO, "Entered selectApp\n");

    /* Only clear the highlight if it's different to the icon we're going to. */
    if ( idx != currentIdx )
    {
        /* Clear highlight from currently highlighted icon. */
        appEntry = dbGetEntry(currentIdx,0);
        if (appEntry != NULL )
        {
            piboxLogger(LOG_INFO, "Call to clear highlighted icon: currentIdx = %d\n", currentIdx);
            do_drawing(darea[currentIdx], appEntry, FALSE, 2);
        }
    }

    /* Highlight the newly selected icon. */
    appEntry = dbGetEntry(idx,0);
    if (appEntry != NULL )
    {
        piboxLogger(LOG_INFO, "Calling to highlight new icon: idx = %d\n", idx);
        do_drawing(darea[idx], appEntry, FALSE, 1);
    }
    currentIdx = idx;

#ifndef XEON
#ifndef PIPLAYER
    // Show the app splash
    piboxLogger(LOG_INFO, "Calling draw_splash idx: %d\n", idx);
    draw_splash(NULL, NULL, GINT_TO_POINTER(currentIdx));

    // Update the description field
    if ( appEntry->description != NULL )
        gtk_text_buffer_set_text(appDesc, appEntry->description, -1);
#endif
#endif
}

/*
 *========================================================================
 * Name:   notify
 * Prototype:  void notify( void )
 *
 * Description:
 * Sends a packet to the appManager to tell it to start an app.
 *
 * Arguments:
 * gint idx     Index into list to send.
 * gint noshow  If set, search the noshow list instead of the active list.
 *========================================================================
 */
void 
notify(gint idx, gint noshow)
{      
    int                 sockfd = 0;
    int                 hdr;
    int                 size;
    int                 n;
    APP_T               *appEntry;
    struct sockaddr_in  serv_addr; 

    // Get the app entry for this index
    appEntry = dbGetEntry(idx,noshow);
    if (appEntry == NULL )
    {
       piboxLogger(LOG_ERROR, "No appEntry for index %d\n", idx);
       return;
    } 

    // Initialization of buffers
    memset(&serv_addr, '0', sizeof(serv_addr)); 

    // Create a socket for the message
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        piboxLogger(LOG_ERROR, "Could not create socket to appMgr: %s\n", strerror(errno));
        return;
    } 

    // Setup TCP connection to appMgr port
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(APPMGR_PORT); 

    // appMgr must be running on localhost
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        piboxLogger(LOG_ERROR, "inet_pton error: %s\n", strerror(errno));
        return;
    } 

    // Connect to appMgr
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       piboxLogger(LOG_ERROR, "Connect Failed %s \n", strerror(errno));
       return;
    } 

    // Build a packet
    // 8bit header = 00000001
    hdr = 0x00000001;

    // command string
    if ( appEntry->command == NULL )
    {
       piboxLogger(LOG_ERROR, "Missing command for app: %s\n", appEntry->appName);
       return;
    } 
    piboxLogger(LOG_INFO, "Command to run: %s\n", appEntry->command);

    // 8bit size = length of command
    size = strlen(appEntry->command);

    // Send it
    n = write(sockfd,&hdr,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing hdr: %s", strerror(errno));
         return;
    }
    n = write(sockfd,&size,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing size: %s", strerror(errno));
         return;
    }
    n = write(sockfd,appEntry->command,size);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing command: %s", strerror(errno));
         return;
    }

    // Close the socket
    close(sockfd);
}

#if 0
/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start an application.
 *========================================================================
 */
static gboolean 
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{      
    piboxLogger(LOG_INFO, "Button: x %f, y %f\n", event->x, event->y);
    switch(event->type) {
        case GDK_BUTTON_PRESS:
            if ( event->button == 1 )
            {
                // select active item.
                enableTouch = 0;
                notify(currentIdx, 0);
                piboxLogger(LOG_INFO, "Disabling touch support.\n");
                return(TRUE);
            }
            break;
        default:
            break;
    }
    return(TRUE);
}
#endif

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start an application.
 *========================================================================
 */
static gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    int         idx = currentIdx;
    int         noshow = 0;

    piboxLogger(LOG_INFO, "Key: %s\n", gdk_keyval_name(event->keyval));
    switch(event->keyval) 
    {
        case GDK_Left:
        case GDK_KP_Left:
            piboxLogger(LOG_INFO, "Left key\n");
            idx--;
            break;

        case GDK_Right:
        case GDK_KP_Right:
            piboxLogger(LOG_INFO, "Right key\n");
            idx++;
            break;

        case GDK_Up:
        case GDK_KP_Up:
            piboxLogger(LOG_INFO, "Up key\n");
            idx -=3;
            break;

        case GDK_Down:
        case GDK_KP_Down:
            piboxLogger(LOG_INFO, "Down key\n");
            idx +=3;
            break;

        case GDK_Return:
        case GDK_KP_Enter:
            piboxLogger(LOG_INFO, "Return key\n");
            enableTouch = 0;
            notify(currentIdx, 0);
            return(TRUE);
            break;

        // Ctrl-k moves left and cycles back to the bottom.
        case GDK_k:
            idx--;
            if ( idx < 0 )
                idx = maxApps-1;
            break;

        // "L" and spacebar increments and cycles back to the top.
        case GDK_l:
        case GDK_space:
            idx++;
            if ( idx > maxApps-1 )
                idx = 0;
            break;

        // Ctrl-t opens a terminal window.
        case GDK_t:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Terminal key\n");

                /* Search both lists for the terminal emulator */
                idx = dbFindEntry("Terminal Emulator", noshow);
                if ( idx == -1 )
                    idx = dbFindEntry("Terminal Emulator", ++noshow);
                if ( idx != -1 )
                {
                    enableTouch = 0;
                    notify(idx, noshow);
                }
                else
                    piboxLogger(LOG_ERROR, "Can't find Terminal Emulator app.\n");
            }
            return(TRUE);
            break;

        /*
         * Ctrl-r rotates the display.
         * This is useful on TFT's if it's not oriented correctly.
         */
        case GDK_r:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Rotation key\n");
                piboxMsg(MT_SYS, MA_ROTATE, 0, NULL, 0, NULL, NULL, NULL);
                msg("Rebooting to rotate the display.", 15);
            }
            return(TRUE);
            break;

        default:
            piboxLogger(LOG_INFO, "Key: %s\n", gdk_keyval_name(event->keyval));
            break;
    }

    // Clamp the idx
    if ( idx < 0 )
        idx = 0;
    else if ( idx > maxApps-1 )
        idx = maxApps-1;

    // Update the display
    selectApp(idx);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   initApp
 * Prototype:  void initApp( gint idx )
 *
 * Description:
 * Called when window is exposed (like the first time it is opened) to
 * make sure the first app is highlighted correctly.
 *========================================================================
 */
static gboolean 
initApp(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    selectApp(currentIdx);
    return(TRUE);
}

#ifdef XEON
/*
 *========================================================================
 * Name:   createWindowXeon
 * Prototype:  GtkWidget *createWindowXeon( void )
 *
 * Description:
 * Creates the status bar and icon region for the display.
 *========================================================================
 */
GtkWidget *
createWindowXeon()
{
    GtkWidget           *window;
    GtkWidget           *vbox;
    GtkWidget           *hbox1;
    GtkWidget           *dwga;
    gint                row, col, idx;
    guint               appCount;
    guint               numRows;
    guint               numCols;
    guint               statusHeight=0;
    guint               statusWidth=0;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    window_expose_id = g_signal_connect(G_OBJECT(window), "expose_event", G_CALLBACK(initApp), NULL); 

    /* Support keyboards even if on a touchscreen. */
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    window_keypress_id = g_signal_connect(G_OBJECT(window),
            "key_press_event",
            G_CALLBACK(key_press),
            NULL);

    /* Two rows: status and apps */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_container_add (GTK_CONTAINER (window), vbox);

    statusArea = gtk_drawing_area_new();
    statusWidth = workarea.width;
    statusHeight = STATUS_H;
    gtk_widget_set_size_request(statusArea, statusWidth, statusHeight);
    status_expose_id = g_signal_connect(G_OBJECT(statusArea), "expose_event", G_CALLBACK(draw_status), NULL); 
    gtk_box_pack_start (GTK_BOX (vbox), statusArea, TRUE, TRUE, 0);
    piboxLogger(LOG_INFO, "Status w/h: %d / %d\n", statusWidth, statusHeight);

    /* Two columns: app icons and information */
    hbox1 = gtk_hbox_new (TRUE, 0);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_box_pack_start (GTK_BOX (vbox), hbox1, TRUE, TRUE, 0);

    /*
     * Calculate the width and height of the icon area.
     * Icon height is width/5 and number of rows is height / (width/5).
     */
    appCount = dbGetLength(0);
    piboxLogger(LOG_INFO, "Number of apps: %d\n", appCount);
    numCols = 5;

    cellWidth = workarea.width / numCols;
    cellHeight = cellWidth;
    piboxLogger(LOG_INFO, "Cell w/h: %d / %d\n", cellWidth, cellHeight);

    numRows = (workarea.height - statusHeight) / cellHeight;
    piboxLogger(LOG_INFO, "Number of rows x cols: %d x %d\n", numRows, numCols);

    /* First column: table of application icons */
    iconTable = gtk_table_new(numRows,numCols,TRUE);
    gtk_table_set_row_spacings(GTK_TABLE(iconTable),0);
    gtk_table_set_col_spacings(GTK_TABLE(iconTable),0);
    gtk_widget_set_size_request(iconTable, workarea.width-splashWidth, workarea.height-statusHeight);
    gtk_box_pack_start (GTK_BOX (hbox1), iconTable, TRUE, TRUE, 0);
    piboxLogger(LOG_INFO, "Icon table w/h: %d / %d\n", workarea.width-splashWidth, workarea.height-statusHeight);

    /*
     * A set of Application Icon Drawing Areas
     */
    darea = (GtkWidget **)calloc(1, sizeof(GtkWidget) * (numRows * numCols + 1) );
    idx = 0;
    for (row=0; row<numRows; row++)
    {
        for (col=0; col<numCols; col++)
        {
            dwga = gtk_drawing_area_new();
            gtk_widget_set_size_request(dwga, cellWidth, cellHeight);
            g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_icon), GINT_TO_POINTER(idx)); 
            gtk_table_attach_defaults(GTK_TABLE(iconTable), dwga, col, col+1, row, row+1 );
            darea[idx] = dwga;
            idx++;
        }
    }

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), workarea.width, workarea.height);
    gtk_window_set_title(GTK_WINDOW(window), "Launcher");

    return window;
}
#else
/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a scrolled list of icons on the left and currently
 * selected app information on the right.  
 *
 * Notes:
 * If compiled with -DXEON or -DPIPLAYER then we don't use the splash screen to save 
 * screen space.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *window;
    GtkWidget           *vbox;
    GtkWidget           *hbox1;
    GtkWidget           *dwga;
    gint                row, col, idx;
    guint               appCount;
    guint               numRows;
    guint               numCols;
    guint               statusHeight=0;
    guint               statusWidth=0;

#ifndef PIPLAYER
    GtkWidget           *vbox1;
    GtkWidget           *view;
    PangoFontDescription *fd = NULL;
#endif

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    window_expose_id = g_signal_connect(G_OBJECT(window), "expose_event", G_CALLBACK(initApp), NULL); 

    /* Support keyboards even if on a touchscreen. */
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    window_keypress_id = g_signal_connect(G_OBJECT(window),
            "key_press_event",
            G_CALLBACK(key_press),
            NULL);

    /* Two rows: status and apps */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_container_add (GTK_CONTAINER (window), vbox);

    statusArea = gtk_drawing_area_new();
    statusWidth = workarea.width;
    statusHeight = STATUS_H;
    gtk_widget_set_size_request(statusArea, statusWidth, statusHeight);
    status_expose_id = g_signal_connect(G_OBJECT(statusArea), "expose_event", G_CALLBACK(draw_status), NULL); 
    gtk_box_pack_start (GTK_BOX (vbox), statusArea, TRUE, TRUE, 0);
    piboxLogger(LOG_INFO, "Status w/h: %d / %d\n", statusWidth, statusHeight);

    /* Two columns: app icons and information */
    hbox1 = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_box_pack_start (GTK_BOX (vbox), hbox1, TRUE, TRUE, 0);

    /* 
     * How many rows in the table do we need?
     * We only support 9 apps total for now.
     */
    appCount = dbGetLength(0);
    piboxLogger(LOG_INFO, "Number of apps: %d\n", appCount);
    if ( appCount < 4 )
    {
        numRows = 1;
        numCols = appCount;
    }
    else if ( appCount < 7 )
    {
        numRows = 2;
        numCols = 3;
    }
    else
    {
        numRows = 3;
        numCols = 3;
    }
    piboxLogger(LOG_INFO, "Number of rows: %d\n", numRows);

#ifndef PIPLAYER
    // splashWidth = SPLASH_W;
    splashWidth = (int)(workarea.width/2);
    splashHeight = (int)(workarea.height-STATUS_H-TEXT_H);
    piboxLogger(LOG_INFO, "Splash w/h: %d / %d\n", splashWidth, splashHeight);
#endif

    cellWidth = (workarea.width - splashWidth) / numCols;
    cellHeight = (workarea.height - STATUS_H) / numRows;
    piboxLogger(LOG_INFO, "Cell w/h: %d / %d\n", cellWidth, cellHeight);

    /* First column: table of application icons */
    iconTable = gtk_table_new(numRows,numCols,TRUE);
    gtk_table_set_row_spacings(GTK_TABLE(iconTable),0);
    gtk_table_set_col_spacings(GTK_TABLE(iconTable),0);
    gtk_widget_set_size_request(iconTable, workarea.width-splashWidth, workarea.height-statusHeight);
    gtk_box_pack_start (GTK_BOX (hbox1), iconTable, FALSE, FALSE, 0);
    piboxLogger(LOG_INFO, "Icon table w/h: %d / %d\n", workarea.width-splashWidth, workarea.height-statusHeight);

#ifndef PIPLAYER
    /* Seconds column: Splash image and description */
    vbox1 = gtk_vbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (hbox1), vbox1, TRUE, TRUE, 0);
#endif

    /*
     * A set of Application Icons
     */
    darea = (GtkWidget **)calloc(1, sizeof(GtkWidget) * (numRows * numCols + 1) );
    idx = 0;
    for (row=0; row<numRows; row++)
    {
        for (col=0; col<numCols; col++)
        {
            dwga = gtk_drawing_area_new();
            // gtk_widget_set_size_request(dwga, cellWidth, cellHeight);
            g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_icon), GINT_TO_POINTER(idx)); 
            gtk_table_attach_defaults(GTK_TABLE(iconTable), dwga, col, col+1, row, row+1 );
            darea[idx] = dwga;
            idx++;
        }
    }

#ifndef PIPLAYER
    /*
     * The Application Splash 
     */
    appSplash = gtk_drawing_area_new();
    gtk_widget_set_size_request(appSplash, splashWidth, splashHeight);
    splash_realize_id = g_signal_connect(G_OBJECT(appSplash), "expose_event", G_CALLBACK(draw_splash), NULL);
    gtk_box_pack_start (GTK_BOX (vbox1), appSplash, TRUE, TRUE, 0);

    /*
     * The Application Description 
     */
    view = gtk_text_view_new();
    gtk_widget_set_size_request(view, splashWidth, TEXT_H);
    piboxLogger(LOG_INFO, "text view height: %d\n", TEXT_H);
    appDesc = gtk_text_view_get_buffer( GTK_TEXT_VIEW(view) );
    fd = pango_font_description_from_string ("Italic 20");
    gtk_widget_modify_font (view, fd);
    pango_font_description_free (fd);
    gtk_text_view_set_left_margin (GTK_TEXT_VIEW(view), 10);
    gtk_text_view_set_right_margin (GTK_TEXT_VIEW(view), 10);
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(view), GTK_WRAP_WORD);
    gtk_text_view_set_justification (GTK_TEXT_VIEW(view), GTK_JUSTIFY_CENTER);
    gtk_text_view_set_editable (GTK_TEXT_VIEW(view), FALSE);
    gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW(view), FALSE);
    gtk_text_view_set_pixels_below_lines (GTK_TEXT_VIEW(view), 20);

    gtk_box_pack_start (GTK_BOX (vbox1), view, FALSE, FALSE, 0);
#endif

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), workarea.width, workarea.height);
    gtk_window_set_title(GTK_WINDOW(window), "Launcher");

    return window;
}
#endif

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Bring it down gracefully. */
            gtk_main_quit();
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            gtk_main_quit();
            break;

        case SIGINT:
            /* Bring it down gracefully. */
            gtk_main_quit();
            break;

        case SIGQUIT:
            /* Bring it down gracefully. */
            gtk_main_quit();
            break;

        case SIGUSR1:
            /* App manager signals us so we can re-enable touch processing. */
            piboxLogger(LOG_INFO, "Appmgr signaled SIGUSR1.\n");
            if ( isCLIFlagSet( CLI_TOUCH ) )
            {
                piboxLogger(LOG_INFO, "Re-enabling touch support.\n");
                enableTouch = 1;
            }

            // Wait a second, then update the icons so the active one is highlighted. 
            g_timeout_add(500, (GSourceFunc)timer_exec, NULL);
            break;
    }
}

/*
 * ========================================================================
 * Name:   timer_exec
 * Prototype:  gboolean timer_exec( GtkWidget *window )
 *
 * Description:
 * Highlight the initial selection.
 *
 * Notes:
 * This is a one time timer - it is called right after the window opens
 * because the window expose event is overridden by the drawing area
 * expose events.
 * ========================================================================
 */
static gboolean
timer_exec(GtkWidget * window)
{
    piboxLogger(LOG_INFO, "Updating display.\n");
    selectApp(currentIdx);
    return(FALSE);
}

/*
 * ========================================================================
 * Name:   status_exec
 * Prototype:  gboolean status_exec( GtkWidget *window )
 *
 * Description:
 * Update date and time.
 * ========================================================================
 */
static gboolean
status_exec(GtkWidget * window)
{
    draw_status(NULL, NULL, NULL);
    return(TRUE);
}

/*
 * ========================================================================
 * Name:   network_watcher
 * Prototype:  gboolean network_watcher( void *dummy )
 *
 * Description:
 * Check that network is available.
 * ========================================================================
 */
static gboolean
network_watcher(void *dummy)
{
    pthread_mutex_lock(&networkMutex);
    network_avail = isNetworkUp();
    pthread_mutex_unlock(&networkMutex);
    return(TRUE);
}

/*
 * ========================================================================
 * Name:   init_icons
 * Prototype:  void init_icons( void )
 *
 * Description:
 * Load some non-app icons used in the display.
 * ========================================================================
 */
static void
init_icons(void )
{
    if ( isCLIFlagSet( CLI_TEST) )
    {
        piboxLogger(LOG_INFO, "Loading NETWORK OFF from %s\n", T_NETWORK_OFF);
        icons[0].iconImage = gdk_pixbuf_new_from_file(T_NETWORK_OFF, NULL);
    }
    else
    {
        piboxLogger(LOG_INFO, "Loading NETWORK OFF from %s\n", I_NETWORK_OFF);
        icons[0].iconImage = gdk_pixbuf_new_from_file(I_NETWORK_OFF, NULL);
    }
    icons[0].iwidth    = gdk_pixbuf_get_width(icons[0].iconImage);
    icons[0].iheight   = gdk_pixbuf_get_height(icons[0].iconImage);

    if ( isCLIFlagSet( CLI_TEST) )
    {
        piboxLogger(LOG_INFO, "Loading NETWORK ON from %s\n", T_NETWORK_ON);
        icons[1].iconImage = gdk_pixbuf_new_from_file(T_NETWORK_ON, NULL);
    }
    else
    {
        piboxLogger(LOG_INFO, "Loading NETWORK ON from %s\n", T_NETWORK_ON);
        icons[1].iconImage = gdk_pixbuf_new_from_file(I_NETWORK_ON, NULL);
    }
    icons[1].iwidth    = gdk_pixbuf_get_width(icons[1].iconImage);
    icons[1].iheight   = gdk_pixbuf_get_height(icons[1].iconImage);
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget           *window;
    struct stat         stat_buf;
    char                path[MAXBUF];
    struct sigaction    sa;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {   
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR1.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGINT.\n", PROG);
        exit(1);
    }

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    /* Load the environment variables file, if any. */
    piboxLoadEnv(NULL);

    /* Identify RPi hardware. */
    cliOptions.platform = piboxGetPlatform();
    piboxLogger(LOG_INFO, "Platform: %d\n", cliOptions.platform);
    
    /* Dump some configuration status */
    piboxLogger(LOG_INFO, "Verbosity level: %d\n", cliOptions.verbose);
    if ( cliOptions.flags & CLI_LOGTOFILE )
        piboxLogger(LOG_INFO, "Logfile: %s\n", cliOptions.logFile);
    if ( cliOptions.flags & CLI_ROTATE )
        piboxLogger(LOG_INFO, "Rotation enabled.\n");

    // Set the path to the where the application description files live.
    memset(path, 0, MAXBUF);
    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(path, "%s", DBTOP_T);
    else
        sprintf(path, "%s", DBTOP);

    // Test for db existance.
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory");
        return(1);
    }

    // Change to that directory
    chdir(path);

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));

    // Load db
    maxApps = dbLoad(path, 0);
    if ( maxApps == 0 )
    {
        piboxLogger(LOG_ERROR, "No applications found.\n");
        return -1;
    }

    // Load the noshow options.  These are XML configs that we don't
    // display with the other icons.
    sprintf(path, "%s/.noshow", DBTOP);
    dbLoad(path, 1);

    /* Get display config information */
    gtk_init(&argc, &argv);
    loadDisplayConfig();
    piboxLogger(LOG_INFO, "display type: %s\n", 
            (piboxGetDisplayType()==PIBOX_LCD)?"LCD":
            (piboxGetDisplayType()==PIBOX_HDMI)?"HDMI":
            (piboxGetDisplayType()==PIBOX_TFT)?"TFT":"Unknown");

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(iconTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }
    else
        piboxLogger(LOG_INFO, "Not a touch screen device.\n");

    /* Load non-app icons used in the display. */
    init_icons();

    // Initialize gtk, create its windows and display them.
    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse("./gtkrc");

#ifdef XEON
    window = createWindowXeon();
#else
    window = createWindow();
#endif

    /* Resize icons and selection mask. */
    resizeIcons(cellWidth, cellHeight);
    generateIconHighlight();
#ifndef XEON
#ifndef PIPLAYER
    resizeSplash(splashWidth, splashHeight-10);
#endif
#endif

    gtk_widget_show_all(window);
    sleep(3);
    piboxLogger(LOG_INFO, "Displayed window wxh: %d x %d\n", 
            gdk_window_get_width(window->window),
            gdk_window_get_height(window->window));

    /* expose event on the window doesn't add the highlight so force it with a timer. */
    g_timeout_add(1500, (GSourceFunc)timer_exec, NULL);
    clockID = g_timeout_add(1000, (GSourceFunc)status_exec, NULL);
    g_timeout_add(10*1000, (GSourceFunc)network_watcher, NULL);

    gtk_main();

    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        /* Touch processor blocks in a read, so we need to SIGINT to break out of it. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}
